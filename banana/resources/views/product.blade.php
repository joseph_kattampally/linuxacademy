@extends('master')

@section('content')

<?php //print_r($productoptions);exit;?>
    <div class="container">
        @foreach ($productoptions->chunk(3) as $items)
            <div class="row">
                @foreach ($items as $product)
                    <div class="col-md-4">
                        <div class="row text-center" style="margin:10px;">
                            <img src="{{ asset('img/' . $product->image) }}" class="center-block img-responsive" width="150" alt="product">
                        </div>
                      <h2 class="row text-center">{{$product->name}}</h2>
                      <h3 class="row text-center">{{$product->screensize}}</h3>
                      <h3 class="row text-center">{{$product->screentype}}</h3>
                      <h3 class="row text-center">{{$product->cameraresolution}}</h3>
                      <h3 class="row text-center">{{$product->cameratype}}</h3>
                      <h3 class="row text-center">{{$product->authenticationtype}}</h3>
                      <h3 class="row text-center">{{$product->chip}}</h3>
                      <h3 class="row text-center">{{$product->chargingtype}}</h3>

                      <form action="{{ url('/cart') }}" method="POST" class="side-by-side">
                          {!! csrf_field() !!}
                          <input type="hidden" name="id" value="{{ $product->id }}">
                          <input type="hidden" name="image" value="{{ $product->image }}">
                          <input type="hidden" name="name" value="{{ $product->name }}">
                          <input type="hidden" name="price" value="{{ $product->price }}">
                          <input type="hidden" name="shippingtype" value="">
                          <input type="hidden" name="shippingrate" value="">
                          <input type="hidden" name="discountminnumber" value="">
                          <input type="hidden" name="discountrate" value="">
                          <input type="hidden" name="maxlimit" value="">
                          <input type="submit" class="btn btn-success btn-lg" value="Add to Cart">
                      </form>

                      <form action="{{ url('/wishlist') }}" method="POST" class="side-by-side">
                          {!! csrf_field() !!}
                          <input type="hidden" name="id" value="{{ $product->id }}">
                          <input type="hidden" name="image" value="{{ $product->image }}">
                          <input type="hidden" name="name" value="{{ $product->name }}">
                          <input type="hidden" name="price" value="{{ $product->price }}">
                          <input type="hidden" name="shippingtype" value="">
                          <input type="hidden" name="shippingrate" value="">
                          <input type="hidden" name="discountminnumber" value="">
                          <input type="hidden" name="discountrate" value="">
                          <input type="hidden" name="maxlimit" value="">
                          <input type="submit" class="btn btn-primary btn-lg" value="Add to Wishlist">
                      </form>
                    </div>
                @endforeach
              </div>
         @endforeach
    </div> <!-- end container -->

@endsection
