@extends('master')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif


        <div class="jumbotron text-center">
            @foreach ($products->chunk(4) as $items)
                <div class="row">
                    @foreach ($items as $product)
                        <h1 class="display-3">{{ $product->name }}</h1>

                        <div class="row text-center" style="margin:10px;">
                            <img src="{{ asset('img/' . $product->image) }}" class="center-block img-responsive" width="300" alt="product">
                        </div>

                        <p class="lead">{{ $product->description }}</p>
                        <p><a class="btn btn-lg btn-success" href="{{ url('/shop/' . $product->id ) }}" role="button">Pre-Order Now</a></p>
                    @endforeach
                </div> <!-- end row -->
            @endforeach
        </div>
    </div> <!-- end container -->

@endsection
