<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productoptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('productid');
            $table->string('name');
            $table->decimal('price', 10, 2);
            $table->string('image');
            $table->string('screensize');
            $table->string('screentype');
            $table->string('cameraresolution');
            $table->string('cameratype');
            $table->string('authenticationtype');
            $table->string('chip');
            $table->string('chargingtype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productoptions');
    }
}
