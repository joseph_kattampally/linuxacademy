<?php

use Illuminate\Database\Seeder;

class DiscountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discount')->insert([
            'productid' => '1',
            'minnumber' => '3',
            'rate' => '10.00',
        ]);
    }
}
