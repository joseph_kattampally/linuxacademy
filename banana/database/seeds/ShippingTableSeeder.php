<?php

use Illuminate\Database\Seeder;

class ShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping')->insert([
            'productid' => '1',
            'shippingtype' => 'Fragile shipping',
            'rate' => '100.00',
        ]);
    }
}
