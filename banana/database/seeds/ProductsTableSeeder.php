<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'uPhone',
            'slug' => 'uPhone',
            'description' => 'Our vision has always been to create an uPhone that is entirely screen. One so immersive the device itself disappears into the experience. And so intelligent it can respond to a tap, your voice, and even a glance. With uPhone, that vision is now a reality. Say hello to the future.',
            'image' => 'uPhone.jpeg',
            'limit' => '6',
        ]);
    }
}
