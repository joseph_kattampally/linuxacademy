<?php

use Illuminate\Database\Seeder;

class ProductoptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productoptions')->insert([
            'productid' => '1',
            'name' => 'Mini',
            'price' => '800.00',
            'image' => 'uPhone-mini.jpeg',
            'screensize' => '4.9',
            'screentype' => 'Rexina HD display',
            'cameraresolution' => '15MP',
            'cameratype' => 'Wide-angle and telephoto cameras',
            'authenticationtype' => 'Touch ID',
            'chip' => 'A12 Bionic Chip',
            'chargingtype' => 'Wireless charging',
        ]);

        DB::table('productoptions')->insert([
            'productid' => '1',
            'name' => 'XL',
            'price' => '1100.00',
            'image' => 'uPhone-xl.jpeg',
            'screensize' => '5.9',
            'screentype' => 'Rexina HD display',
            'cameraresolution' => '15MP',
            'cameratype' => 'Wide-angle and telephoto cameras',
            'authenticationtype' => 'Touch ID',
            'chip' => 'A12 Bionic Chip',
            'chargingtype' => 'Wireless charging',
        ]);

        DB::table('productoptions')->insert([
            'productid' => '1',
            'name' => 'Glass',
            'price' => '2100.00',
            'image' => 'uPhone-glass.jpeg',
            'screensize' => '6.9',
            'screentype' => 'Ultra Rexina HD display',
            'cameraresolution' => '25MP',
            'cameratype' => '3D Matterport cameras',
            'authenticationtype' => 'Face ID',
            'chip' => 'A12 Bionic Chip',
            'chargingtype' => 'Wireless charging',
        ]);
    }
}
