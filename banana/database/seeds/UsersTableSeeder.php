<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Banana User',
            'email' => 'user@banana.com',
            'password' => '$2y$10$sps3lY7qJ2GSg32piqgYnusgmD4Wdpn4knVh.VafwVF.f/rwP6x.6',
        ]);
    }
}
